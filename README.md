# OpenML dataset: US-Real-Estate-Listings-by-Zip-Code

https://www.openml.org/d/43631

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
Real Estate inventory of listings from 2012-2017
Content
Includes data for all Real Estate listings in the US, such as,  active listings, prices, days on market, price changes, and pending listings by county.

Median Listing Price: The median listing price within the specified geography during the specified month.
Active Listing Count: The count of active listings within the specified geography during the specified month. The active listing count tracks the number of for sale properties on the market, excluding pending listings where a pending status is available. This is a snapsot measure of how many active listings can be expected on any given day of the specified month.
Median Days on Market: The median number of days property listings spend on the market within the specified geography during the specified month. Time spent on the market is defined as the time between the initial listing of a property and either its closing date or the date it is taken off the market.
New Listing Count: The count of new listings added to the market within the specified geography. The new listing count represents a typical weeks worth of new listings in a given month. The new listing count can be multiplied by the number of weeks in a month to produce a monthly new listing count.
Price Increase Count: The count of listings which have had their price increased within the specified geography. The price increase count represents a typical weeks worth of listings which have had their price increased in a given month. The price increase count can be multiplied by the number of weeks in a month to produce a monthly price increase count.
Pending Listing Count: The count of pending listings within the specified geography during the specified month, if a pending definition is available for that geography. This is a snapsot measure of how many pending listings can be expected on any given day of the specified month.

Acknowledgements
This data set and other similar (including details on all columns) can be found here: Realtor.com 
Inspiration
I downloaded this data to research it and make a best decision when buying a home in Austin, TX.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43631) of an [OpenML dataset](https://www.openml.org/d/43631). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43631/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43631/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43631/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

